package schoolproject.dbcontroller;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import schoolproject.models.TestModel;

/**
 * Created by Alexander Hems alexander.hems@t8y.com on 05.10.2016.
 */
@Transactional
public interface TestDao extends CrudRepository<TestModel, Long> {

    public TestModel findByName(String name);
}
