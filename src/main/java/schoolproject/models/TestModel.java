package schoolproject.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Alexander Hems alexander.hems@t8y.com on 05.10.2016.
 */
@Entity
@Table(name = "test")
public class TestModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    private String name;

    public TestModel() {
    }

    public TestModel(long id) {
        this.id = id;
    }

    public TestModel(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
