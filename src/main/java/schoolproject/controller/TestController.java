package schoolproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import schoolproject.dbcontroller.TestDao;
import schoolproject.models.TestModel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Alexander Hems alexander.hems@t8y.com on 05.10.2016.
 */
@RestController
public class TestController {


    // Hier wird der Dao angebunden um in zu verwenden können
    @Autowired
    private TestDao testDao;

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/test")
    public List<TestModel> test(@RequestParam(value="name", defaultValue = "") String name){
        ArrayList<TestModel> tests = new ArrayList<>();
        if(name.isEmpty()){
            Iterator<TestModel> iterator = testDao.findAll().iterator();
            while(iterator.hasNext()){
                tests.add(iterator.next());
            }
        }else{
            TestModel byName = testDao.findByName(name);
            tests.add(byName);
        }
        return tests;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/test")
    public void createUser(@RequestParam(value="name", defaultValue="World") String name){
        TestModel testModel = new TestModel(name);
        testDao.save(testModel);
    }

}
